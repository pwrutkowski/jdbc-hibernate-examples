package com.sda.hibernate.commons.dao;


import com.sda.hibernate.commons.connection.HibernateUtil;
import com.sda.hibernate.commons.entity.Movie;
import com.sda.hibernate.commons.entity.Person;
import com.sda.hibernate.commons.entity.PersonMovie;
import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.List;

public class MovieDao {

    private final Logger logger = Logger.getLogger(MovieDao.class);

    public Movie findById(Integer id) {
        Transaction transaction = null;

        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            transaction = session.beginTransaction();

            Movie movie = session.get(Movie.class, id);
            transaction.commit();
            return movie;
        } catch (HibernateException e) {
            if (transaction != null)
                transaction.rollback();

            logger.error(e.getMessage(), e);
        }



        return null;
    }

    public Movie findByName(String title) {

        Transaction transaction = null;

        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            transaction = session.beginTransaction();

            Movie result = (Movie) session.createQuery("FROM Movie a WHERE a.title = : title")
                    .setParameter("title", title)
                    .getSingleResult();

            transaction.commit();
            return result;
        } catch (HibernateException e) {
            if (transaction != null)
                transaction.rollback();

            logger.error(e.getMessage(), e);
        }
        return null;
    }


    public void save(Movie movie) {

        Transaction transaction = null;

        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            transaction = session.beginTransaction();
            //session.saveOrUpdate(movie);
            session.save(movie);
            transaction.commit();
        } catch (HibernateException e) {
            if (transaction != null)
                transaction.rollback();

            logger.error(e.getMessage(), e);
        }
    }


    public void delete(Movie movie) {
    }

    public void update(Movie movie) {
    }

    public List<PersonMovie> findAll() {
        return null;
    }

    public List<Movie> findAllMovies() {
        return null;
    }

    public List<Person> findAllActorsForMovieNativeSQL(Integer movieId) {
        return null;
    }

    public List<Person> findAllActorsForMovieHQL(Integer movieId) {
        return null;
    }

    public List<Person> findAllActorsForMovieCriteriaAPI(Integer movieId) {
        return null;
    }

    public List<Movie> findAllMoviesForActorNativeSQL(String firstName, String lastName) {
        return null;
    }

    public List<Movie> findAllMoviesForActorHQL(String firstName, String lastName) {
        return null;
    }

    public List<Movie> findAllMoviesForActorCriteriaAPI(String firstName, String lastName) {
        return null;
    }
}
