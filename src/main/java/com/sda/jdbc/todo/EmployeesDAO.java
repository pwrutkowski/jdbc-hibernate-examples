package com.sda.jdbc.todo;


import com.sda.jdbc.commons.connection.CustomConnection;
import com.sda.jdbc.commons.connection.MySqlConnector;
import com.sda.jdbc.commons.entity.Employee;
import lombok.extern.log4j.Log4j;
import org.apache.log4j.Logger;

import java.sql.*;
import java.util.List;

@Log4j

public class EmployeesDAO {

    private CustomConnection mySqlConnection; //tworzymy tu sobie żeby w ogóle łączyło z bazą danych

    // private static final Logger logger = Logger.getLogger(MySqlConnector.class);  //dajemy log4j lombokiem
    public EmployeesDAO() {
        mySqlConnection = new MySqlConnector(); //inicjujemy w konstruktorze
    }

    public Employee findById(int employeeId) {
        String query = "SELECT * FROM employees where employee_id =" + employeeId;

        log.info(query);

        try (Connection connection = mySqlConnection.getConnection();
             Statement statement = connection.createStatement();
             ResultSet resultSet = statement.executeQuery(query);) {

            if (resultSet.next()) {  //result set głównie do odbioru ale da się go przerobić do zapisu
                log.info(resultSet.getString("first_name"));

                Employee employee = new Employee();
                employee.setEmployeeId(resultSet.getInt("employee_id"));
                employee.setFirstName(resultSet.getString("first_name"));
                employee.setLastName(resultSet.getString("last_name"));
                employee.setEmail(resultSet.getString("email"));
                employee.setJobId(resultSet.getString("job_id"));
                employee.setSalary(resultSet.getDouble("salary"));
                employee.setHireDate(resultSet.getDate("hire_date").toLocalDate());
                employee.setCommissionPct(resultSet.getDouble("commission_pct"));
                return employee;
            }

        } catch (SQLException sqlException) {
            log.error(sqlException.getMessage());
        }
        return null;
    }

    public void delete(int employeeId) {
        String query = "DELETE FROM employees WHERE employee_id = ?";


        try (Connection connection = mySqlConnection.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(query);) {

            /*
            Statement statement = connection.createStatement();
            statement.executeUpdate(query);
             */

            preparedStatement.setInt(1, employeeId);
            preparedStatement.executeUpdate();

            log.info(preparedStatement);

        } catch (SQLException sqlException) {
            log.error(sqlException.getMessage());
        }
    }

    public void save(Employee employee) {
        String query = "INSERT INTO employees " +
                "(EMPLOYEE_ID," +
                "FIRST_NAME," +
                "LAST_NAME," +
                "EMAIL," +
                "HIRE_DATE," +
                "JOB_ID," +
                "SALARY)"  +
                "VALUES" +
                "(?,?,?,?,?,?,?)";

        //log.info(query);

        try (Connection connection = mySqlConnection.getConnection();
             PreparedStatement statement = connection.prepareStatement(query);) {

            statement.setInt(1, employee.getEmployeeId());             //employee id
            statement.setString(2, employee.getFirstName());            //first name
            statement.setString(3, employee.getLastName());             //last name
            statement.setString(4, employee.getEmail());                //email
            statement.setDate(5, Date.valueOf(employee.getHireDate()));
            statement.setString(6, employee.getJobId());                //job id
            statement.setDouble(7, employee.getSalary());               // salary


            statement.executeUpdate();

            log.info(statement);

        } catch (SQLException sqlException) {
            log.error(sqlException.getMessage());
        }
    }

    public void update(Employee employee) {
        String query = "UPDATE employees SET email = ? WHERE employee_id = ?";


        try (Connection connection = mySqlConnection.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(query);) {

            /*
            Statement statement = connection.createStatement();
            statement.executeUpdate(query);
             */

            preparedStatement.setString(1, employee.getEmail());
            preparedStatement.setInt(2, employee.getEmployeeId());
            preparedStatement.executeUpdate();

            log.info(preparedStatement);

        } catch (SQLException sqlException) {
            log.error(sqlException.getMessage());
        }
    }

    public List<Employee> findAll() {
        return null;
    }

    public void saveBatch(List<Employee> employees) {
    }

    public void deleteBatch(List<Employee> employees) {
    }

}
