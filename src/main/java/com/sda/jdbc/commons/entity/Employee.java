package com.sda.jdbc.commons.entity;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@AllArgsConstructor
@NoArgsConstructor

public class Employee {
    private int employeeId;
    private String firstName;
    private String lastName;
    private String email;
    private String jobId;
    private double salary;
    private LocalDate hireDate;
    private double commissionPct;



}
